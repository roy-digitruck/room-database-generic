package com.example.room_test.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.room_test.dao.UserDao
import com.example.room_test.model.User

@Database(entities = [User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}
