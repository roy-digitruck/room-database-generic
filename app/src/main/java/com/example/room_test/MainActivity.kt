package com.example.room_test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.room_test.database.AppDatabase
import com.example.room_test.model.User

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Thread {
            val db = Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java, "user-db"
            ).build()

            val dao = db.userDao()

            val user = User()
            user.firstName = "Shikhor"
            user.lastName = "Roy"

            dao.save(user)
            dao.findAll().forEach { user -> println(user) }
        }.start()
    }
}
