package com.example.room_test.dao

import androidx.room.Dao
import com.example.room_test.model.User


@Dao
abstract class UserDao : BaseDao<User>()
