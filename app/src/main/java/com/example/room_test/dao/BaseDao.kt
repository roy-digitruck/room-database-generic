package com.example.room_test.dao

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import java.lang.reflect.ParameterizedType

abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun save(data: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveAll(vararg dataList: T): LongArray

    @Update
    abstract fun update(data: T)

    @Update
    abstract fun updateAll(vararg data: T)

    @Delete
    abstract fun delete(obj: T)

    fun deleteAll(): Int {
        val query = SimpleSQLiteQuery("delete from $tableName")
        return doDeleteAll(query)
    }

    fun findAll(): List<T> {
        val query = SimpleSQLiteQuery("select * from $tableName")
        return doFindAll(query)
    }

    fun findById(id: Long): T {
        val query = SimpleSQLiteQuery("select * from $tableName where id = ?", arrayOf<Any>(id))
        return doFindById(query)
    }

    @RawQuery
    protected abstract fun doFindAll(query: SupportSQLiteQuery): List<T>

    @RawQuery
    protected abstract fun doFindById(query: SupportSQLiteQuery): T

    @RawQuery
    protected abstract fun doDeleteAll(query: SupportSQLiteQuery): Int

    private val tableName: String
        get() {
            val clazz = (javaClass.superclass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<*>
            return clazz.simpleName
        }
}
