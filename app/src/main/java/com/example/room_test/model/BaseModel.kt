package com.example.room_test.model

import androidx.room.PrimaryKey

abstract class BaseModel {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
